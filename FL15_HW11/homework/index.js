let isEquals = (a, b) => a === b;
console.log(isEquals(3, 3));


let isBigger = (a, b) => a > b;
console.log(isBigger(5, -1));


let storeNames = (...arg) => arg;
console.log(storeNames('Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy'));



let getDifference = (a, b) => a > b ? a - b : b - a;
console.log(getDifference(5, 3));
console.log(getDifference(5, 8));


let negativeCount = (arr) => arr.filter((el) => el < 0).length;
console.log(negativeCount([4, 3, 2, 9]));
console.log(negativeCount([0, -3, 5, 7]));

function letterCount(a, b) {
	let arr = [];
	for (let i = 0; i < a.length; i++) {
		if (a[i] === b) {
			arr.push(b);
		}
	}
	return arr.length;
}

console.log(letterCount('Marry', 'r'));
console.log(letterCount('Barny', 'y'));
console.log(letterCount('', 'z'));


function countPoints(arr){
	return arr.reduce(function(sum,score){
		const firstNum= Number(score.split(':')[0]);
		const secondNum= Number(score.split(':')[1]);
		if(firstNum > secondNum){
			sum=sum+3;
		}
		if(firstNum < secondNum){
			sum=sum+0;
		}
		if(firstNum === secondNum){
			sum=sum+1;
		}
		return sum;
	}, 0);

}

console.log(countPoints(['100:90', '110:98', '100:100', '95:46', '54:90', '99:44', '90:90', '111:100']));
