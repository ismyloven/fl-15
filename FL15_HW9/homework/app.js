function reverseNumber(num) {
	let number = String(num);
	let newNumber = '';
	let negativeNumb = '';
	if (number > 0) {
		for (let i = number.length - 1; i >= 0; i--) {
			newNumber = newNumber + number[i];
		}
	}
	if (number < 0) {
		for (let i = number.length - 1; i >= 1; i--) {
			negativeNumb = negativeNumb + number[i];
		}
		newNumber = '-' + negativeNumb;
	}
	return +newNumber;
}


function forEach(arr, func) {
	for (i = 0; i < arr.length; i++) {
		func(arr[i], i, arr);
	}
}

function map(arr, func) {
	const newMap = [];
	forEach(arr, function (arr) {
		newMap.push(func(arr));
	});
	return newMap;

}

function filter(arr, func) {
	const filterMap = [];
	forEach(arr, function (arr) {
		if (func(arr)) {
			filterMap.push(arr);
		}
	});
	return filterMap;
}


function getAdultAppleLovers(data) {
	let userAge = 18;
	let names = [];
	let fruitLover = [];
	filter(data, function (data) {
		if (data['favoriteFruit'] === 'apple' && data['age'] > userAge) {
			names.push(data);
		}
	});

	map(names, function (names) {
		fruitLover.push(names['name']);
	});

	return fruitLover;
}



function getKeys(obj) {
	let objKey = [];
	for (let key in obj) {
		objKey.push(key);
	}
	return objKey;
}


function getValues(obj) {
	let objValues = [];
	for (let value in obj) {
		objValues.push(obj[value]);
	}
	return objValues;
}


function showFormattedDate(dateObj) {
	let year = dateObj.getFullYear();
	let month = dateObj.getMonth();
	let monthName = ['Yan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	let day = dateObj.getDate();
	return `It is ${day} of ${monthName[month]}, ${year}`;
}


