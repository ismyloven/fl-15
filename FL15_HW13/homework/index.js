const birthday22 = new Date(2000, 9, 22);
const birthday23 = new Date(2000, 9, 23);

function getAge(birthDate) {
	let now = new Date(2020, 9, 22);
	let ageDiff = now.getFullYear() - birthDate.getFullYear();
	let monthDiff = now.getMonth() - birthDate.getMonth();
	let dayDiff = now.getDate() - birthDate.getDate();

	if (monthDiff === 0 && dayDiff < 0) {
		ageDiff = ageDiff - 1;
	}

	return ageDiff;
}

getAge(birthday22);
getAge(birthday23);


function getWeekDay(date) {

	if (Date.now()) {
		date = new Date(2020, 9, 22);
	}
	let days = ['Sunday', 'Monday', 'Tuesday', ' Wednesday', 'Thursday', 'Friday', 'Saturday'];

	return days[date.getDay()];

}

getWeekDay(Date.now());
getWeekDay(new Date(2020, 9, 22));


function getAmountDaysToNewYear(date) {
	let lastDayOfThisYear = new Date(2020, 11, 31);
	let day = 1000 * 60 * 60 * 24;
	let diff = lastDayOfThisYear - date;

	return Math.ceil(diff / day);

}

getAmountDaysToNewYear(new Date(2020, 7, 30));
getAmountDaysToNewYear(new Date(2020, 0, 1));

function getProgrammersDay(inComeYear) {

	let date = new Date(inComeYear, 0, 1);
	date.setDate(date.getDate() + 256 - 1);
	let monthName = ['Yan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	let month = date.getMonth();
	let days = ['Sunday', 'Monday', 'Tuesday', ' Wednesday', 'Thursday', 'Friday', 'Saturday'];
	let day = date.getDate();
	let dayWeek = date.getDay();

	return `${day} ${monthName[month]}, ${inComeYear} (${days[dayWeek]})`;

}


getProgrammersDay(2020);
getProgrammersDay(2019);


function howFarIs(specifiedWeekday) {
	let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	let now = new Date();
	let day = now.getDay();
	let weekDayFromValue = specifiedWeekday[0].toUpperCase() + specifiedWeekday.slice(1);
	let indexDay = days.indexOf(weekDayFromValue);

	if (day > indexDay) {
		let dayToDate = indexDay + (days.length - day);
		return `It's ${dayToDate} day(s) left till ${weekDayFromValue}`;
	}
	if (day === indexDay) {
		return `Hey, today is ${weekDayFromValue} =)`;
	}
	if(day < indexDay) {
		let dayToDate = indexDay - day;
		return `It's ${dayToDate} day(s) left till ${weekDayFromValue}`;
	}


}

howFarIs('friday');
howFarIs('Thursday');


function isValidIdentifier(value) {
	let regexValue = /\b^[a-zA-z$_]{1}([{Alpha}$\w+\d+]){0,}$/;
	let validValue = regexValue.test(value);

	return validValue;

}


isValidIdentifier('myVar!');
isValidIdentifier('myVar$');
isValidIdentifier('myVar_1');
isValidIdentifier('1_myVar');


const testStr = 'My name is John Smith. I am 27.';

function capitalize(testStr) {
	testStr = testStr.replace(/(^\w{1}|\s{1}\w)/g, testStr => testStr.toUpperCase());
	return testStr;
}

capitalize(testStr);


function isValidAudioFile(audio) {
	let regexValue = /^\b[a-zA-Z]{0,2}[^_][a-zA-Z]{1,}\.(?:flac|mp3|alac|aac)$\b/;
	let validValue = regexValue.test(audio);

	return validValue;


}

isValidAudioFile('file.mp4');
isValidAudioFile('my_file.mp3');
isValidAudioFile('file.mp3');


const testString = 'color: #3f3; background-color: #AA00ef; and: #abcd';

function getHexadecimalColors(str) {
	let newArray = [];
	let regExp = /#([a-f0-9]{3}){1,2}\b/gi;

	if (str.match(regExp) === null) {

		return newArray;
	}
	return str.match(regExp);

}


getHexadecimalColors(testString);
getHexadecimalColors('red and #0000');


function isValidPassword(value) {

	let regExp = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]{8,}/g;

	let validPassword = regExp.test(value);

	return validPassword;
}


isValidPassword('agent007');
isValidPassword('AGENT007');
isValidPassword('AgentOOO');
isValidPassword('Age_007');
isValidPassword('Agent007');


function addThousandsSeparators(value) {

	let newValue = value.toString();

	return newValue.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}


addThousandsSeparators('1234567890');
addThousandsSeparators(1234567890);


const text1 = 'We use https://translate.google.com/ to translate some words and phrases from https://angular.io/ ';
const text2 = 'JavaScript is the best language for beginners!';


function getAllUrlsFromText(sometext) {
	let newArray = [];
	let regExp = /(https?:\/\/[^\s]+)/g;
	sometext = sometext.match(regExp);

	if (sometext === null) {

		return newArray;
	}
	return sometext;


}

