function isFunction(functionToCheck) {
	return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}

const pipe = (value, ...funcs) => {

	let currenValue = value;

	try {
		for (let i = 0; i < funcs.length; i++) {
			if (isFunction(funcs[i])) {
				currenValue = funcs[i](currenValue);
			} else {
				throw new TypeError(`Provided argument at position ${i} is not a function!`);
			}

		}
	} catch (er) {

		return er;
	}
	return currenValue;
};


const replaceUnderscoreWithSpace = (value) => value.replace(/_/g, ' ');

const capitalize = (value) =>
	value
		.split(' ')
		.map((val) => val.charAt(0).toUpperCase() + val.slice(1))
		.join(' ');

const appendGreeting = (value) => `Hello, ${value}!`;

// const error = pipe('john_doe', replaceUnderscoreWithSpace, capitalize, '');

//alert(error); // Provided argument at position 2 is not a function!

const result = pipe(
	'john_doe',
	replaceUnderscoreWithSpace,
	capitalize,
	appendGreeting
);

alert(result);

