function visitLink(path) {
	let visit = localStorage.getItem(path);
	let currentValue = visit ? parseInt(visit) : 0;
	let count = currentValue + 1;
	return localStorage.setItem(path, count);
}

function viewResults() {
	let content = document.getElementById('content');
	let node = document.createElement('p');
	content.appendChild(node).innerHTML = `<ul>
    <li>You visited Page1 ${localStorage.getItem('Page1') || 0} time(s)</li>
    <li>You visited Page2 ${localStorage.getItem('Page2') || 0} time(s)</li>
    <li>You visited Page3 ${localStorage.getItem('Page3') || 0} time(s)</li>
    </ul>`;
	localStorage.clear();
}


