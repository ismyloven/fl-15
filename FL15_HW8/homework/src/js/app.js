let signEvent = prompt('Organizing a meeting', 'meeting');

if (signEvent) {
	let container = document.querySelector('.container');
	container.style.visibility = 'visible';
	saveResult();
} else {
	alert('Refresh page and Try again');
}

function saveResult() {
	let firstName = document.querySelector('.firstName');
	let enterTime = document.querySelector('.enterTime');
	let enterPlace = document.querySelector('.enterPlace');
	let regexTime = /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/;
	let form = document.querySelector('.form');
	let confirmButton = document.querySelector('.confirm-button');
	let converterButton = document.querySelector('.converter-button');

	form.addEventListener('submit', function (elem) {
		elem.preventDefault()
	});

	confirmButton.addEventListener('click', handler);
	converterButton.addEventListener('click', converterCurrency);

	function handler() {
		let validTime = regexTime.test(enterTime.value);
		if (firstName.value !== '' && enterPlace.value !== '' && enterTime.value !== '') {
			let firstPart = `${firstName.value} has a ${signEvent} today at`;
			let secondPart = `${enterTime.value} somewhere in ${enterPlace.value}`;
			console.log(`${firstPart} ${secondPart}`);
		} else {
			alert('Input all data');
		}

		if (!validTime) {
			alert('Enter time in format hh:mm');
		}
	}

	function converterCurrency() {
		let euroSum = prompt('Enter amount of euro');
		let dollarSum = prompt('Enter amount of dollar');

		let oneEuro = 33.52;
		let oneDollar = 27.76;
		let fixNum = 2;
		let validSum = euroSum >= 0 && dollarSum >= 0 && isFinite(euroSum) && isFinite(dollarSum);

		if (validSum) {
			let euroEqual = (euroSum * oneEuro).toFixed(fixNum);
			let dollarEqual = (dollarSum * oneDollar).toFixed(fixNum);
			alert(`${euroSum} euros are equal ${euroEqual} hrns, ${dollarSum} dollars are equal ${dollarEqual} hrns`);
		} else {
			alert('Please enter valid values');
		}

	}

}



