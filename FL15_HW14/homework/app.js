const appRoot = document.getElementById('app-root');

appRoot.innerHTML =
	`
<div class="header">
<h1>Countries Search</h1>
<form class="form">
<div class="div-form">
<p>Please choose type of search:</p>
<div class="label-div">
<label for="radio1"><input type="radio" id="radio1" onclick="handler()" name="radio1">By region</label>
<label for="radio2"><input type="radio" id="radio2" onclick="handler()" name="radio1">By Language</label>
</div>
</div>
<label for="country">Please choose search query:
<select id="country" value="country" selected="selected"></select>
</label>
</form>
</div> `

let select = document.getElementById('country');
let optionByDefolt = document.createElement('option');
optionByDefolt.textContent = 'Select Value';
select.appendChild(optionByDefolt);

let radio = document.getElementsByName('radio1');

function handler() {
	let val;
	for (let i = 0; i < radio.length; i++) {
		if (radio[i].checked) {
			val = i;
		}
	}
	if (val === 0) {
		byRegion();

	}
	if (val === 1) {
		byLanguage();
	}

	return val;
}

function byRegion() {
	let arrayRegion = externalService.getRegionsList();
	select.innerHTML = `<select>Select Value</select>`;
	for (let key of arrayRegion) {
		let option = document.createElement('option');
		option.textContent = `${key}`;
		option.setAttribute('value', `${key}`);
		select.appendChild(option);
	}
}

function byLanguage() {
	let languageRegion = externalService.getLanguagesList();
	select.innerHTML = `<select>Select Value</select>`;
	for (let key of languageRegion) {
		let option = document.createElement('option');
		option.textContent = `${key}`;
		option.setAttribute('value', `${key}`);
		select.appendChild(option);
	}
}


select.addEventListener('click', drawTable);

function drawTable() {

	let arrayCountry = externalService.getCountryListByRegion(this.value);
	let arrayLanguage = externalService.getCountryListByLanguage(this.value);

	if (handler() === 0) {
		renderTable(arrayCountry);
	}
	if (handler() === 1) {
		renderTable(arrayLanguage);
	}
}

function renderTable(arrayCountry) {
	let table = document.createElement('table');
	table.className = 'table';
	appRoot.append(table);
	let elementTable = document.getElementsByClassName('table');

	for (let i = 0; i < elementTable.length - 1; i++) {
		appRoot.removeChild(elementTable[i]);
	}
	let tr = document.createElement('tr');
	tr.className = 'tr-header';
	table.append(tr);
	let arrayTableHeader = ['name ↑, flagURL, region, area ↕, capital, languages'].join(',').split(',');


	arrayTableHeader.forEach(item => {
		let countryTd = document.createElement('td');
		countryTd.className = 'countryTd-class';
		if (item === 'name' || item === ' area') {
			item = `<button> ${item} &#8595;</button>`;
		}
		countryTd.textContent = item;
		tr.appendChild(countryTd);

	});

	arrayCountry.forEach(item => {
		let countryTr = document.createElement('tr');
		countryTr.className = 'countryTr-class';
		table.appendChild(countryTr);
		Object.values(item).forEach(elem => {
			let item = elem;
			let substring = '.png';

			if (typeof elem === 'string' && elem.includes(substring)) {
				item = `<img src='${elem}' alt='flag'>`;
			}

			if (typeof elem === 'object') {

				item = Object.values(elem)
			}

			let testTd = document.createElement('td');
			testTd.className = 'countryTd-class';
			testTd.innerHTML = item;
			countryTr.appendChild(testTd);
		});
	});


}








