let table = document.querySelector('.table');
table.addEventListener('click', paintCell);

function paintCell(event) {
	let target = event.target;
	if (target.tagName === 'TD') {
		highlight(target);
	}
	let blue = target.closest('TR');
	let blueObj = blue.firstChild;

	if (blueObj.nextSibling === target) {
		blue.classList.toggle('blue');
		if (target.className === 'special') {
			target.classList.toggle('green');
		}
	}
}

function highlight(td) {

	if (td) {

		td.classList.toggle('yellow');

	}
	if (td.classList.contains('special')) {

		td.classList.toggle('green');
	}
}

let phone = document.querySelector('.input-phone');
phone.addEventListener('click',getValidPhone );

function getValidPhone() {
	let value = document.getElementById('elem').value;
	let button = document.getElementById('btn');
	let input = document.getElementById('elem');
	let result = document.getElementById('result');
	let reg = new RegExp(/^\+380[0-9]{9}$/);
	if (reg.test(value)) {
		button.disabled = false;
		input.classList.add('green-input');
		input.classList.remove('red-input');
		result.innerHTML = `<p class='green-result'>Data was succesfully sent</p>`;

	} else {
		button.disabled = true;
		input.classList.add('red-input');
		input.classList.remove('green-input');
		result.innerHTML = `<p class='red-result'>Type number does not follow format +380*********</p>`;
	}
}





let basketball = document.querySelector('.basketball');
let ball = document.querySelector('.ball');
let countX = document.querySelector('.page-x');
let countY = document.querySelector('.page-y');
basketball.addEventListener('click', startPlay);


function startPlay(event) {

	const fieldCoords = event.currentTarget.getBoundingClientRect();
	let ballCoords = {
		top: event.clientY - fieldCoords.top - basketball.clientTop - ball.clientHeight / 2,
		left: event.clientX - fieldCoords.left - basketball.clientLeft - ball.clientWidth / 2
	};
	if (ballCoords.top < 0) {
		ballCoords.top = 0;
	}

	if (ballCoords.left < 0) {
		ballCoords.left = 0;
	}
	if (ballCoords.left + ball.clientWidth > basketball.clientWidth) {
		ballCoords.left = basketball.clientWidth - ball.clientWidth;
	}

	if (ballCoords.top + ball.clientHeight > basketball.clientHeight) {
		ballCoords.top = basketball.clientHeight - ball.clientHeight;
	}

	ball.style.left = ballCoords.left + 'px';
	ball.style.top = ballCoords.top + 'px';

	if (event.target === countY) {

		let teamA = document.querySelector('.teamA');
		teamA.value++;

	}
	if (event.target === countX) {
		let teamB = document.querySelector('.teamB');
		teamB.value++;
	}
}





