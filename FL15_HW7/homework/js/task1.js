let cash = function () {
	let fix = 2;
	let money = 1000;
	let count = 100;
	let initialCash = Number(prompt('Enter the amount of money', ''));
	let age = Number(prompt('Enter the number of years', ''));
	let percent = Number(prompt('Enter the percentage for the year', ''));

	if (initialCash >= money && age >= 1 && Number.isInteger(age) && percent <= count && percent >= 1) {
		let profit = initialCash;
		for (let i = 0; i < age; i++) {
			initialCash = initialCash * percent / count + initialCash;
		}
		alert(` Initial amount: ${profit}
Number of years: ${age}
Percentage of year: ${percent}

Total profit: ${(initialCash - profit).toFixed(fix)}
Total amount: ${initialCash.toFixed(fix)}`);

	} else {
		alert('Invalid input!!');
	}
};

cash();